// -----------------------------------

// always attempt to break to a new line in a way that
// preserves a minimum number of words per line
import "./patches/minwordsperline.js";

// click and drag to scroll the page
import "./patches/dragtoscroll.js";

// convert markdown to HTML tags
import "./patches/markdowntohtml.js"

// -----------------------------------

// import helper patch for binding shortcuts to choices
import choices from "./patches/shortcuts/choices.js";

// autosave
import "./patches/autosave.js";

// allow stepping the story forwards and backwards
import "./patches/stepback.js"

Patches.add(function() 
{
	Shortcuts.add("q", this.stepBack);
});


// options
options.linedelay = 600.0;
options.passagedelay = 200.0;
options.showlength = 1000.0;
options.hidelength = 750.0;
options.suppresschoice = 0.0;

// create our game
var story = new Story("main.ink");