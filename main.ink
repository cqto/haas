// STATE
// Four levels of awareness, 0 (unaware), 1 (suspicious), 2 (worried), 3 (scared)
VAR awareness = 0
VAR reason_for_signing = ""
VAR dominant_hand = ""
VAR breakfast_choice = ""
VAR drank_juice = false
VAR phone_in_locker = false
VAR heart_limit_reached = false
VAR took_walk = false
VAR remembered_jingle = false
VAR saw_terms_and_conditions = true
VAR saw_memory = false


// GAME FLOW
-> controls ->
# CLEAR
-> prologue -> keep -> day_1 -> keep -> epilogue -> keep -> ending -> END

// CONTROLS
=== controls === 
This is a non-linear story, where you must make choices.

The progress is automatically saved, but you can go back whenever you want.

Click an option to select it.
Press Q to go back.

* [Start]
->->


// COMMON KNOTS

=== keep ===
+ [Continue]
- #CLEAR
->->

=== what_to_do ===
{~What do you want to do?|What now?|Let's see...}
->->

=== head_home ===
{~Better to go home|Let's go home|You head home|You go home}
->->

=== alarm_clock ===
\*ring ring*
{-> alarm_normal | -> alarm_late | -> alarm_too_late}

= alarm_normal
* [Turn off the alarm]
- You quickly turn off the alarm. It's 8:30.
->->

= alarm_late
The sound pierces through your ears, but doesn't reach you.
*   [Turn off the alarm]
- You try to reach the clock, but it's a bit harder today.
*   [Try again]
- With some effort, you manage to turn it off.
It's 9:40.
->->

= alarm_too_late
{~The clock keeps ringing.|It's been hours since it started.|\*ring ring*|Shut up.} 
* [Make it stop]
- it stops. 
It's too late.
->->

=== haas_welcome ===
Good morning. I'm Sandra, your HAAS AI assistant.
While I load your profile, tell me, how do you feel?
* Very bad
* Bad
* Normal
* Good
* Very good
- Thanks! 
Now let's have a look at your data.
->->

=== heart_limit_notification(times, locker) ===
{not locker:
    {times > 1:
        #CLEAR
        \*bzzzz bzzzz*
        You pick your phone.
    }
    {times:
        - 2:    [HAAS - You have reached 75% of your HS limit]
        - 3:    [HAAS - You have reached the limit of your HS]
                ~ heart_limit_reached = true
    }
}
-> keep ->
-   ->->

=== hand_choice ===
* [Left hand]
    ~ set(dominant_hand, "left")
* [Right hand]
    ~ set(dominant_hand, "right")
- ->->


// PROLOGUE

=== prologue ===

= setting
Let's go back for a second. To a few months ago.
You are...
*   [drunk.]
    ~ reason_for_signing = "drunk"
    That's right, you're drunk. And alone, too.
    After that last conversation with whom now's your ex, you decided to go wild.
    And hoo boy, you've gone wild.
    You look at your...
    -> hand_choice ->
    
    There's a phone in you disgusting, {dominant_hand} hand.
    This phone isn't even yours, how did you end up with it?
    But wait...
    There's a number dialed in.
    *   *   [Call it]
            How are you going to ignore this brilliant opportunity to be miserable?
    *   *   [Ignore it]
            Who do you think you are? You've drunk enough to be able to resist your urges.
    
*   [checking your company bonuses.]
    ~ reason_for_signing = "company"
    Your work just added a new private healthcare and insurance option as a bonus for employees.
    You hate it there, but hey, sometimes it's convenient to work in the industry.
    Oddly enough, the only way to contact this health is through phone, so you reach it with your...
    -> hand_choice -> 
    <> {dominant_hand} hand and tap the number.
    
*   [watching a video from your favourite tech influencer.]
~ reason_for_signing = "hype"
    "...but do YOU know what's the FUTURE? Today's sponsor".
    You're about to skip the ad, as you always do, but your hands are full of orange dust.
    And now you're glad you didn't skip it, because it truly is THE FUTURE of healthcare.
    Without a second thought, you grab your phone with your...
    -> hand_choice ->
    With your dirty, {dominant_hand} hand, and tap the number.

- Seconds after calling, a robotic, warm voice speaks to you.
"Welcome to HAAS, the future of healthcare. How can I help you?"

*   [Answer]
# CLEAR
-The conversation lasts for <> 
{reason_for_signing:
    - "drunk": an eternity. The sun has exploded, and you've definitely throwed up too many times.
    - "company": almost your whole pause. You should be getting back to work in a few minutes.
    - "hype": just a few minutes. You're far too excited to think about it.
}

At last, the voice asks for your confirmation.
"The contract will be sent to the email you provided. Please, sign and return it to confirm your HAAS subscription."

The call ends and, as the robotic woman said, your phone buzzes. It's the contract.
* [Sign it.]

- {reason_for_signing:
    - "drunk": It's impressive that you managed to do all that in your condition, but you made it!
    - "company": ...done. You still have a few minutes left, just enough for a coffee.
    - "hype": You're now ready for THE FUTURE! If LonisTechTools recommends this, it must be incredible.
}
And within a minute, you receive the final answer.
"Congratulations, you are now a HAAS customer. We hope you enjoy your new and healthier life with us!"
->->

// DAY

=== day_1 ===
#CLEAR
-> good_morning -> keep ->
-> chatbot -> keep ->
-> morning -> keep ->
-> day_1_daily_life ->
->->

= good_morning
-> alarm_clock ->
Finally back home, it's been rough to spend that many weeks at the hospital.
Luckily, the recovery was smooth, and you don't have any sequels. You can now forget about the accident, as long as you follow your medical recommendations. 
And now out of the bed, it's time to check that assistant app your doctor told you.
->->

= chatbot
-> haas_welcome ->
- (menu)
    *   [Perform health check]
        ...
        Everything looks good! No irregularities found.
        Keep up the good work! Don't forget to keep a healthy lifestyle and follow advice regarding your rehabilitation process.
        -> menu

    *   [View medical recommendations]
        These are the main pieces of advice regarding your rehabilitation process:
        \- Keep a balanced diet.
        \- Be active.
        \- Maintain regular sleeping habits.
        -> menu
    +   [Leave]
        Goodbye!
-   ->->

= morning
You start wallking down the stairs, hungry and still half asleep. 
What will you have for breakfast today? Is there even milk in the fridge? 
You're just a few steps away from your breakfast, but that's enough to get lost in your hungry thoughts...
...And fall down the stairs.
*   [GRAB!]
- Your head was getting ready for the painful hit, but without noticing, your {dominant_hand} hand quickly grabbed the handrail. 
Talk about reflexes.

Safely, you arrive at the kitchen. And your depressively empty fridge offers you...
*   [Suspicious milk]
    ~ breakfast_choice = "milk"
    ...sure, what could go wrong?
*   [A small pear]
    ~ breakfast_choice = "pear"
    Almost the only healthy thing in you house.
*   [Slightly mouldy bread]
    ~ breakfast_choice = "bread"
    There's not that much mould, you can eat the rest of it, right?
-   It's been a few days since you came back home from the hospital, but so many things have happened...
Maybe it's time to get back to a routine.
...
And buy some food.
->->

// DAILY LIFE

=== day_1_daily_life ===
After having what can barely be considered as a breakfast, you leave your home, ready for going back to a healthy routine. 

- (map) {What do you want to do?|What will you do now?|What do you feel like doing?|}
    *   [Go to the supermarket]
        -> supermarket
    *   [Go to the library]
        -> library
    *   [Go to the gym]
        -> gym
    * ->
-   ->->
    
= supermarket
The supermarket is crowded today. More than a store, it looks like a music festival.
"The JumboMart yearly sale! EVERYTHING half price!", you read in a big sign, as you manage to go inside the store. Suddenly, something grabs your attention...

- (supermarket_choice)
*   [Your ex is waiting in the queue, with some of your friends.]
    Fuck.
    You're human, mistakes happen. And that was a biiig one.
    Not only that, but it's also painful. To be so close, to see these people, with whom you've shared so much, here at the store, having fun without you. 
    You get lost in your thoughts, but not for long, because you're about to hit... -> supermarket_choice
*   [A giant orange with legs.]
    "MumboJumbo.", says the big sphere, as it turns towards you. Its lifeless, green eyes look deep into your soul. Its big hands guard a tiny table, with some small glasses full with juice. And its big, rigid mouth hides the face of a tired woman. 
    A very tired woman.
    "Mumbo Mumbo. MumboJuice free sip." 
    
    -   -   (juice_choice)
    *   *   [Take a glass of MumboJuice.]
            You take one of those tiny plastic glasses, and drink what's inside.
            The woman inside MumboJumbo looks at your hand, with an unreadable expression. Suddenly, she whispers:
            "Don't."
            
    *   *   *   [Drink it.]
                ~ drank_juice = true
                ...
                "You made a big mistake", says the woman, with a strange, worried expression. 
                With a strange sensation, you put down the empty glass and go away.
    *   *   *   [Put it back.]
                You slowly put the glass back into the table, in the same spot where you picked it.
                The woman nods. The big orange doesn't.
    *   *   [Ask the woman how she feels.]
            "Not funny.", she says. You won't get far with this approach.
            -> juice_choice
    *   *   [Leave.]
            You're not in the mood for this.
            
- After this disturbing and juicy experience, you suddenly remember that you came here to buy stuff.

- (buy_stuff)
*   [Go to the vegetable section.]
    Time to make your fridge proud again. You pick up tomatoes, lettuce, a squash, and some fruit too.
    {breakfast_choice == "pear": Your pear's sacrifice won't be in vain.|The lonely pear you found this morning will finaly have some company.}
    -> buy_stuff
    
*   [Go to the dairy products section.]
    {
        - breakfast_choice == "milk":
            As soon as you enter the dairy aisle, your stomach starts to growl. Almost as if it remembered the horrible torture you made it endure a few hours ago, when you drank that "milk". 
            ...
            Having learned your lesson,
        - else:
            Cheese! Milk! Yoghurt! Nothing compared to hospital food. With unusual excitement,
    } <> you pick all the dairy products you need.
    -> buy_stuff
*   -> 

- {drank_juice: -> juice_section ->}

- ...and the shopping list is now empty!
With a few quite heavy bags, you won't have to worry about running out of food for a few days. 
->  map


= juice_section
And, why not...
*   [Go to the juice section.]
-   Trying to avoid the eye of MumboJumbo, you slip into the aisle. That tiny sip of juice made you want some more, and after all these days in the hospital you deserve a little treat, right?
*   [GRAB!]
-   Without thinking too much about it, you grab a bottle of MumboJuice with your {dominant_hand} hand.
->->
    
= gym
It's been ages since you last did some physical training, but both your doctor and the AI assistant suggested that it might be really good after your accident.
It's time to make use of that gym membership you've been paying for almost two years.

You change clothe and put your bag in the locker.
Put your phone in too?
*   [Yes]
    ~ phone_in_locker = true
    You've come here to do some exercise, not to waste time reading messages.
*   [No]
    What if someone texts you, or even worse, what if you get bored?
-Finally ready, you see a few machines, a timetable for group classes today and some weights.

- (gym_activities) {What do you want to do?|And now what?|You want MORE???|That's enough.}
*   [Join the next group class]
    You take a look at the timetable. "Cage Challenge", starting in five minutes.
    Sounds tough, but you can do it!
    ...
    The class starts. As the instructor explains everything, you start to regret each and every decision you made since you decided to come here. 
    Every push-up feels like a knife.
    Every jump hurts like treason.
    Every time you bend your knees, they sound like a nut cracking.
    And feeling nothing but pain and exhaustion, the class ends.
    Your muscles can now take a break. Remember that this is good for you.
    This is good for you.
    ...
    Sure.
    -> heart_limit_notification(gym_activities, phone_in_locker) ->
    -> gym_activities
    
*   [Go to the weightlifting section]
    WEIGHTS!
    BUFF PEOPLE! 
    SWEAT!
    You give it your all, and your all now lies on the floor, exhausted.
    A group of nice people were very collaborative, and even told you how to properly hold the different bars and weights.
    But it didn't help your weak muscles that much.
    -> heart_limit_notification(gym_activities, phone_in_locker) ->
    -> gym_activities
    
*   [Go to the cardio section]
    This area is almost full, and there's only two bikes free.
    There's a screen that mimics a trip across the mountains, and ambient sounds too.
    It doesn't help that much to the immersion, but at least it's nice.
    -> heart_limit_notification(gym_activities, phone_in_locker) ->
    -> gym_activities
    
*   {gym_activities > 2} [Leave]
*   ->

- That's enough for today. You're {gym_activities>2: absolutely} exhausted, and it's just your first day back.
Let's catch some fresh air and do something else.
-> map
    
    
= library
They finally opened the new pubic library while you were in the hospital, and it's about time to see if it's as amazing as people is saying.
*   [Take a walk]
    ~ took_walk = true
    The weather is quite nice today. The birds are singing, and without noticing, you join them. A happy and familiar melody comes out of your lungs in the shape of a whistle.
    *   *   [Try to remember where it comes from]
            It's on the tip of your tongue. You've heard it earlier today, for sure.
            Come on, think...
            {
                - supermarket:
                    Aha! It's that MumboJumbo jingle from the supermarket.
                    ~ remembered_jingle = true
                - else:
                    Ugh, forget it. You'll remember eventually.
            }
    *   *   [Go with the flow]
    -   -   With that melody in your lips,
    
*   [Take the bus]
    The bus is crowded, as always.
    -   -   (notifications) {As you open the public transport app, you see a few notifications.|There's more.|Even more.|}
    *   *   [HAAS - update on terms and conditions]
            As you tap the notification, a crazy amount of legal jibberish fills your phone screen.
            It's impossible to understand anything there, so you just close it.
            ~ saw_terms_and_conditions = true
            -> notifications
    *   *   [Work email - 37 unread messages]
            You're not reading that, not until you come back.
            -> notifications
    *   *   [PeopleGO - Hippie season has begun!]
            Didn't you uninstall this game months ago? They kind of ruined it with all the advertising and micropayments.
            -> notifications
    *   *   {notifications < 3} [Delete all]
    *   *   ->
    -   -   ...done.
    With no pending notifications,
    

- <> you arrive at the new library. 
A huge and modern-looking building, much more impressive than you thought.
But let's see if the inside it's as astounding as the outside.

You  walk through the revolving doors and see a mixture of shelves, fancy carts, elevators and escalators.
- (ground_floor) {It's a bit overwhelming, so you...|And now...|What else?|}
*   [Look up]
    You decide to look up, and before noticing, you're lost in layers of poligonal holes are connected with escalators, showing a brute concrete ceiling.
    There's people coming up and down, sitting on colorful couches, playing with their kids. And you're just looking from the ground floor.
    -> ground_floor
*   [Look down]
    You didn't expect to find much looking at the floor, but lines leading to a staircase grab your attention. Not only the library grows upwards, but also has a basement.
    -> ground_floor
*   [Look around]
    You're clearly in the busiest area, with people coming in and out, returning books, hanging out at the cafeteria, and there's even a gift shop.
    This is not just a library.
    -> ground_floor
*   ->
- (explore_library) {After taking a look, you decide to...|Now...|There's nothing left to do here today.}
*   [Go up]
    You don't just go up, but to the last floor. There's an area with larger tables, surrounded by huge glass windows. From there you can see a beautiful landscape.
    As you look at the sea, yous start getting a bit dizzy...
    ...
    You're on the beach. There's a lot of people around you, and it looks like you didn't came alone. You see a few chairs next to your towel, and a big icebox.
    "Mom! Look!"
    A child's voice is trying to reach you, but something's wrong...
    *   *   [You don't have any kids.]
    -   - Something's definitely wrong. This is not just a dream, it looks like...
    A memory.
    So foggy, yet so real. But it's not yours.
    The child comes, and gives you a beautiful shell. And when you grab it, you clearly see...
    *   *   Your body is not yours.[]
    -   -   The child is laughing, and while she comes back to play in the sand, she starts singing a catchy tune.
    You join her, and suddenly...
    
    ...
    Suddenly, you're back in the library. There's a few people looking at you, with relatively angry faces.
    "Please, this is a library. Could you stop whistling???"
    You beg your pardon and move away. That's been a weird experience, for sure. 
    Who's the woman in your dream?
    Did you fall asleep? You weren't even tired.
    And isn't it a bit weird to whistle while daydreaming?
    You go outside with all these thoughts in your mind, but also trying to avoid whistling that familiar tune.
    ~ saw_memory = true
    -> explore_library
    
*   [Go down]
    You won't be seduced by the futuristic ceilings, the outstanding lights and the plethora of sensations. 
    The basement is calling you, like a cave full of secrets. What kind of treasure might await you there?
    {saw_memory:Let's hope it's not weird daydreaming again...}
    Turns out, it's shelves.
    Children books, newspapers, older tomes... It's definitely less impresive than the rest of the library, but it's still impressive, just with the sheer amount of shelves and books in such a modest space.
    -   -   (basement) {You...|Now, you...|And finally, you...|}
    *   *   [Look up]
            There are some stairs which lead to a middle floor on the basement. You don't see anything special but books, a bit more visible than the archive below.
            -> basement
            
    *   *   [Look down]
            An empty, dark floor.
            No more stairs to the unknown.
            -> basement
    *   *   [Look around]
            You go a bit further away from the stairs, and see more shelves, and what looks like an archive. Each column of shelves opens and closes controlled by a central unit, but it's locked by a password.
            You also see a few doors that say "cinema", and a conference room with a few stands and a big screen. 
            -> basement
    *   *   ->
    -   -   There was interesting stuff in the basement after all.
    -> explore_library
    
*   {explore_library >= 1} [Leave]


- It's a really nice place, but you're getting a bit tired, and didn't bring anything to do.
But you'll come back, for sure.
-> map

// EPILOGUE
=== epilogue ===
You've decided to take a walk home. The weather is nice, the birds are singing...
*   But there's something wrong.[] It's been bugging you since you woke up.
You thought it might be that <>
{breakfast_choice:
    -"milk": dubious milk you drank
    -"pear": sad pear you had
    -"bread": mouldy bread you ate
}<>, but that would be a bit lame.


- (weird_things) {You just arrived home, and are about to think about...|About...|About...?|}
*   {drank_juice} [What happened at the supermarket]
    "You made a big mistake". That woman clearly knew something.
    Why did you have that urge to buy juice? You didn't even like it in the first place.
    As if your body craved more of that orange thing.
    *   *   [Go to the kitchen]
    -   -   You open the fridge, and see how your {dominant_hand} hand slowly moves towards the MumboJuice bottle.
    On its own.
    *   *   [Try to grab something else]
            As soon as you grab a pear next to it, countless thoughts fill your head.
            "I feel like drinking some MumboJuice", you think.
            But do you?
            ~   inc(awareness, 1)
    *   *   [Leave it be and see what happens]
            Your hand grabs the juice, and your mouth goes along as well.
            You're suddenly humming that annoying tune again, while opening the plastic bottle.
            And when you think it's time to stop, it's too late.
    
    -   -   There's no doubt.
            Your brain, your mouth, your {dominant_hand} hand... your body acted on its own.
            -> keep -> weird_things

*   {heart_limit_reached} [What happened at the gym]
    {not phone_in_locker:That weird messages you got at the gym|That weird notifications you saw when you picked your phone from the gym locker} seemed a bit suspicious...
    And a bit intrusive.
    What does it even mean to "reach your HS limit"?
    You reach your phone to check the healthcare, but wait...
    There's a few notifications from your bank account.
    *   *   [Check your bank account]
            Reluctantly, you open your bank account, fearing the worst.
            And you were right.
            "[HAAS] Heart Service rate limit reached - automatic bonus purchase"
            "[HAAS] Heart Service rate limit reached - automatic bonus purchase"
            "[HAAS] Heart Service rate limit reached - automatic bonus purchase"
            ...
            There's sixteen more like that, and they're not cheap.
            More important, they shouldn't be there.
            ~inc(awareness, 1)
    *   *   [Ignore it]
            You can check them later...
            ...right?
    -   -   -> keep -> weird_things
    
*   {saw_memory or saw_terms_and_conditions} [What happened at the library]
    {saw_memory: -> memory -> }
    {saw_terms_and_conditions: -> terms ->}
*   ->

- -> keep ->
*   ->
- It's getting late, and confusing.
More exhausted than ever, you go to sleep, and turn off the lights.
->->

= memory
That dream... that child, that woman...
{remembered_jingle: And that horrible MumboJumbo song again|And that familiar, annoying song}.
*   [Try to remember]
        You do your best trying to remember, but there's something else coming.
        More unfamiliar faces, more strange families.
        Weddings, funerals.
        Schools, offices.
        As soon as you try to empty your thoughts, more and more foreign memories come and go.
        It's unbearable.
        ~   inc(awareness, 1)
*   [Ignore it]
        Probably it was just a weird dream, you're just tired.
-   ->->

= terms
    Maybe it has something to do with that notification you got from the HAAS thing?
    It can't be that, right?
*   [Check it]
        "Human As A Service Inc. - Terms and conditions".
        You try to decipher all that legal bullshit. Words like "Heart Service", "DreamAds" and "EmergencyGrab" are all across the document.
        You have a very bad feeling about this.
        ~ inc(awareness, 1)
*   [Pass]
        Nah, you're overthinking.
- ->->


=== ending ===
# CLEAR
And thus, the day comes to an end.
Your consciousness fades away.
Your limbs relax, and your eyes slowly close.
-> keep ->
Then, a circle appears.
It starts to run towards you, with a big smile.
And as MumboJumbo the big orange starts to sing, you realize...
{awareness:
    -0: HAAS is the future of healthcare.
    -1: you need to sleep a bit more.
    -2: your body is not your own.
    -3: you are no more than a puppet.
    -4: you are a slave. 
    A slave of capitalism.
}

#CLEAR
END.
-> END

// UTILS
=== function inc(ref x, k) ===
	~ x = x + k
	
=== function set(ref x, k) ===
    ~ x = k
    
